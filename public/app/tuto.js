var app = angular.module('tuto',['ngRoute','ngMaterial','ngMessages', 'angular-svg-round-progressbar']).run(function($rootScope, $mdSidenav){

});

app.config(function($routeProvider, $mdThemingProvider){

	var customAccent={
		'50': '#FFFFFF',
		'100': '#FFFFFF',
		'200': '#FFFFFF',
		'300': '#FFFFFF',
		'400': '#FFFFFF',
		'500': '#FFFFFF',
		'600': '#FFFFFF',
		'700': '#FFFFFF',
		'800': '#FFFFFF',
		'900': '#FFFFFF',
		'A100': '#FFFFFF',
		'A200': '#FFFFFF',
		'A400': '#FFFFFF',
		'A700': '#FFFFFF'
	};
	var customAccent2 = {
		'50': '#EB0005',
		'100': '#EB0005',
		'200': '#EB0005',
		'300': '#EB0005',
		'400': '#EB0005',
		'500': '#EB0005',
		'600': '#EB0005',
		'700': '#EB0005',
		'800': '#EB0005',
		'900': '#EB0005',
		'A100': '#EB0005',
		'A200': '#EB0005',
		'A400': '#EB0005',
		'A700': '#EB0005'
	};
	var customAccent3 = {
		'50': '#24282B',
		'100': '#24282B',
		'200': '#24282B',
		'300': '#24282B',
		'400': '#24282B',
		'500': '#24282B',
		'600': '#24282B',
		'700': '#24282B',
		'800': '#24282B',
		'900': '#24282B',
		'A100': '#24282B',
		'A200': '#24282B',
		'A400': '#24282B',
		'A700': '#24282B'
	};

/*	$mdThemingProvider
			.definePalette('sotous', customAccent2);
	$mdThemingProvider
			.definePalette('sotousAccent', customAccent2);
	$mdThemingProvider
			.definePalette('sotousWarn', customAccent3);
	$mdThemingProvider.theme('default')
			.primaryPalette('sotous')
			.accentPalette('sotousAccent')
			.warnPalette('sotousWarn');*/
	$routeProvider
	//home page
		.when('/', {
			templateUrl:'home.html',
			controller: 'homeCtrl'
		});
});

app.controller('homeCtrl', function($scope, $interval, $timeout, $http){
	$scope.name="";
	$scope.email="";
	$scope.comment="";
	$scope.invalid=false;
	$scope.showCircle = false;
	$scope.current=0;
	$scope.queryControl=0;
	$scope.usage=0;
	$scope.usageSending=0;
	$scope.sending=0;
	$scope.disabled = true;
	$scope.notice = false;
	$scope.clear=true;
	$scope.buttonClass={};
	$scope.emailNotice={};
	$scope.emailMessage="";
	$scope.sent=false;
	var mytimeout = null;
	var mytimeout1 = null;
	$scope.mytimeout2 = null;

		$scope.onTimeout = function(){
			mytimeout = $timeout($scope.onTimeout, 10);
			$scope.current++;
			if($scope.current>95){
				$scope.stopTimer();
				$scope.restartTimer();
			}
		};

		$scope.onTimeout1 = function(){
			mytimeout = $timeout($scope.onTimeout1, 10);
			$scope.current--;
			if($scope.current<0){
				$scope.stopTimer();
				$scope.startTimer();
			}
		};

		$scope.onTimeout2 = function(){
			mytimeout = $timeout($scope.onTimeout2, 1000);
			$scope.queryControl++;
			if($scope.notice||$scope.invalid){
				$timeout.cancel(mytimeout);
				$timeout.cancel(mytimeout1);
			}

			if($scope.queryControl>10){
				var user = {
					email:$scope.email
				};
					$scope.usage++;
				if($scope.usage>1){}else{
					$http.post('/api/validate/email',user).success(function(data){
						console.log(data);
						$scope.showCircle = false;
						if(data==="failure"){
							$scope.notice = true;
							$scope.emailNotice={
								color:'red'
							};
							$scope.emailMessage="Invalid address."
						}else if(data.is_valid===false){
							if(data.did_you_mean){
								$scope.notice = true;
								$scope.emailNotice={
										color:'red'
								};
								$scope.emailMessage="This email isn't valid. Did you mean "+data.did_you_mean+" ?";
							}else{
								if(data.address){
									$scope.notice = true;
									$scope.emailNotice={
										color:'red'
									};
									$scope.emailMessage="Invalid address."
								}else{
									$scope.notice = true;
									$scope.emailNotice={
										color:'red'
									};
									$scope.emailMessage="You must enter an email address.";
								}
							}
						}else{
							$scope.disabled=false;
							$scope.notice = true;
							$scope.buttonClass = {
								color: 'white',
								background: 'black'
							};
							$scope.emailNotice={
								color:'green'
							};
							$scope.emailMessage="Good! This is a valid email.";
						}
					});
				}			
			}

			if($scope.queryControl>60){
				$scope.showCircle = false;
				$scope.invalid=true;
			}
		};

		$scope.onTimeout3 = function(){
			$scope.mytimeout2 = $timeout($scope.onTimeout3, 1000);
			$scope.sending++;
			console.log($scope.sending);
			if($scope.sent){
				$timeout.cancel($scope.mytimeout2);
			}
			if($scope.sending>10){
			   $scope.usageSending++;
			   if($scope.usageSending>1){}else{
			   	 var to= {
			 	 	email:$scope.email,
			 	 	name:$scope.name,
			 	 	message:$scope.comment
			 	 };
				   $http.post('api/send/message', to).success(function(data){
			 		console.log(data);
				 		$http.post('api/send/confirmation', to).success(function(final){
				 			console.log(final);
				 			if(final==="confirmation sent."){
				 				$scope.stopTimer();
				 				$scope.sent=true;
				 			}
				 		});
		 			});
				}
			}

			if($scope.sending>20){
				$scope.clear=true;
				$scope.sent=false;
				$scope.invalid=true;
				$scope.sending=0;
				$scope.usageSending=0;
				$timeout.cancel($scope.mytimeout2);
			}
		};

		$scope.restartTimer = function(){
			mytimeout = $timeout($scope.onTimeout1, 100);
			$scope.started=true;
		}

		$scope.startTimer = function(){
			mytimeout = $timeout($scope.onTimeout,100);
			$scope.started=true;
		};

		$scope.stopTimer = function(){
			$timeout.cancel(mytimeout);
			$scope.started=false;
		};	

		$scope.startQuery = function(){
			mytimeout1 = $timeout($scope.onTimeout2, 1000);
			$scope.started=true;
		};

		$scope.startSending = function(){
			$scope.mytimeout2 = $timeout($scope.onTimeout3, 1000);
		};

		$scope.validate=function(){
			if($scope.notice||$scope.invalid){
				$scope.started=false;
				$scope.queryControl=0;
				$scope.usage=0;
			}
			if($scope.started){}else{
				$scope.buttonClass={};
				$scope.notice=false;
				$scope.showCircle = true;
				$scope.disabled=true;
				$scope.invalid=false;
				$scope.startTimer();	
				$scope.startQuery();		
			}
		};

	 $scope.sendEmailFrom = function(){
	 	 $scope.startSending();
	 	 $scope.startTimer();
	 	 $scope.notice = false;
	 	 $scope.clear=false;
	 };	
});


