var mgval = require('mailgun-validator')('pubkey-1915a2aaf595e575268d325082d1026c');
var express = require('express');
var router = express.Router();

router.use(function(req, res, next){
	return next();
});

router.route('/email').post(function(req, res){
	mgval.validate(req.body.email, function(err, data) {
		if(req.body.email==="sotous0@gmail.com"){
			console.log("failure... Same email.");
			res.send("failure");
		}else{
		    if (err) {
		      console.log("failure..."+" "+err);
		      return res.status(500).send(data);
		    } else {
		      console.log("Success!")
		      return res.json(data);
		    }
		}
	 });
});

module.exports = router;